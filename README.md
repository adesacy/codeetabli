# L'Établi du Code

## Présentation

Le Code Établi ou l'établi du code se veut un espace de partage d'expérimentations et de pédagogie autour de méthodes de traitements de données appliquées à des recherches en sciences humaines et sociales.


## Credits for the Hugo theme

Created by [Will Faught](https://willfaught.com).
Released under the [MIT License](https://spdx.org/licenses/MIT.html).
Hosted at https://github.com/willfaught/paige.