---
title: À Propos
paige:
  style: |
    #paige-date,
    #paige-date-header,
    #paige-collections,
    #paige-sections,
    #paige-reading-time,
    #paige-toc,
    #paige-pages {
        display: none;
    }
    #paige-title {
        font-size: 3rem;
    }
---

<br>

<div class="container-fluid">
    <div class="justify-content-center row">
        <div class="col col-auto col-lg-7 px-0">
            <p class="lead text-center">Ingénieur au CNRS dans l'infrastructure nationale de recherche <a href="https://www.huma-num.fr">Huma-Num</a> au sein du département de R&D, le <a href="https://www.huma-num.fr/hnlab/">HNLab</a>, en charge du traitement des données du laboratoire et de la conceptualisation et du développement de chaînes de traitement automatique du langage allant de méthodes classiques de textométrie aux plus contemporaines de Deep Learning et d'intelligence artificielle.</p>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="justify-content-center row">
        <div class="col col-auto col-lg-7 px-0">
            <p class="lead text-center">Je mène en parallèle une thèse en littérature qui tente de construire une histoire quantitative et stylistique de la langue romanesque française sur deux siècles. Dans ce cadre, je suis chercheur associé à la Bibliothèque nationale de France. </p>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="justify-content-center row">
        <div class="col col-auto col-lg-7 px-0">
            <p class="lead text-center">À mes heures perdues, je pratique le vélo, la randonnée, la varappe, l'alpinisme, le ski alpin, écris des romans satiriques (prochainement édités) et crée des jeux de cartes (prochainement disponibles en NFT).</p>
        </div>
    </div>
</div>
