---
paige:
  search:
    hide_page: true
  style: |
    #paige-collections,
    #paige-metadata,
    #paige-sections,
    #paige-pages {
        display: none;
    }
    #paige-title {
        font-size: 3rem;
    }
title: "Le Code Établi"
---

<h1 class="justify-content-center row">Le Code Établi</h1>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="Vault.png" %}}</p>
</div>

<div class="container-fluid">
    <div class="justify-content-center row">
        <div class="col col-auto col-lg-7 px-0">
            <p class="lead text-center">Le Code Établi ou l'établi du code se veut un espace de partage d'expérimentations et de pédagogie autour de méthodes de traitements de données appliquées à des recherches en sciences humaines et sociales.</p>
        </div>
    </div>
</div>

<div class="column-gap-3 d-flex display-6 justify-content-center mb-3">
    {{< paige/icon class="bi bi-github" title="GitLab" url="https://gitlab.huma-num.fr/adesacy/codeetabli/" >}}
</div>
