---
date: "2024-05-01"
description: "Florilège"
paige:
  file_link:
    disable: false
  style: |
    #paige-authors,
    #paige-date,
    #paige-keywords,
    #paige-reading-time,
    #paige-series,
    #paige-toc,
    .paige-authors,
    .paige-date,
    .paige-date-header,
    .paige-keywords,
    .paige-reading-time,
    .paige-series,
    .paige-summary {
        display: block;
    }
series: ["florilège"]
tags: ["citations"]
title: "Florilège"
---


<br>

<div class="container-fluid">
    <div class="justify-content-center row">
        <div class="col col-auto col-lg-7 px-0">
            <p class="lead text-center">Pour se divertir un peu de toutes ces histoires de code, nous recueillons ici quelques mots d'esprit, saillies bienvenues, citations inspirantes, raffarinades.</p>
        </div>
    </div>
</div>

<br>
<br>

<blockquote class="blockquote">
  <p>« Les couilles du pape, il faut qu’elles existent mais elle ne doivent surtout pas servir »</p><br>
  <footer class="blockquote-footer mb-1">Dicton de diplomate</footer>
</blockquote>
<br>

<blockquote class="blockquote">
  <p>« Tout bon tyran se doit de briser la réflexion en tuant les gens intelligents, et en abrutissant son peuple dans un moule de croyances qui l'empêchera de réfléchir »</p><br>
  <footer class="blockquote-footer mb-1">La Boétie, <em>Discours de la servitude volontaire</em></footer>
</blockquote>
<br>

<blockquote class="blockquote"><p> « Ce soir, Lucullus dîne chez Lucullus » </p><br>
  <footer class="blockquote-footer mb-1">Plutarque</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Tirez de votre sang un Vendredi du Printemps, mettez-le sécher au four dans un petit pot, comme est dit ci-dessus, avec les deux couillons d’un lièvre et le foie d’une colombe : réduisez le tout en poudre fine, et en faites avaler à la personne sur qui vous aurez quelques dessein, environ la quantité d’une demie dragée ; et si l’effet ne suit pas à la première fois, réitérez jusqu’à trois fois, et vous serez aimé » </p><br>
<footer class="blockquote-footer mb-1"><em>Le solide trésor des merveilleux secrets de la magie naturelle et cabalistique du Petit Albert</em></footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Un homme sur deux est une femme »</p><br>
<footer class="blockquote-footer mb-1">Huguette Bouchardeau</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Le meilleur hôtel, c’est celui à un milliard d’étoiles »</p><br>
<footer class="blockquote-footer mb-1">Patrick Edlinger</footer>
 </blockquote><br>

<blockquote class="blockquote"><p>« Les pâquerettes de cimetière »</p><br>
<footer class="blockquote-footer mb-1">Expression désuète pour désigner les cheveux blancs</footer>
</blockquote><br>

<blockquote class="blockquote">« Il n'est pas de problème dont une absence de solution ne finisse par venir à bout »</p><br>
<footer class="blockquote-footer mb-1">Henri Queuille</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Aimer, c’est donner ce qu’on n’a pas, à quelqu’un qui n’en veut pas »</p><br>
<footer class="blockquote-footer mb-1">Jacques Lacan</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Le seul métier où l'on ne s'ennuie pas, c'est celui que l'on ne fait pas »</p><br>
<footer class="blockquote-footer mb-1">Alain</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Les perdants, comme les autodidactes, ont toujours des connaissances plus vastes que les gagnants, pour gagner, il faut savoir une seule chose et ne pas perdre son temps à les connaître toutes »</p><br>
<footer class="blockquote-footer mb-1">Umberto Eco, <em>Numéro zéro</em></footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Un chef-d’oeuvre de la littérature n’est jamais qu’un dictionnaire en désordre »</p><br> 
<footer class="blockquote-footer mb-1">Citation apocryphe attribuée à Jean Cocteau</em></footer>
</blockquote><br>

<blockquote class="blockquote"><p>« En trou si beau adultère est béni »</p><br>
<footer class="blockquote-footer>Claude Simon</footer></blockquote><br>

<blockquote class="blockquote"><p>« Les méchants chiffres détruisent les belles théories » </p><br>
<footer class="blockquote-footer>Emmanuel Leroy-Ladurie, reprenant une formule de Marc Bloch : « Les méchants faits détruisent les belles théories »</footer></blockquote><br>

<blockquote class="blockquote"><p>« Je regrette beaucoup d'être dans le besoin, mais le déplore d'autant plus que cela m'a empêché de me conformer à mon désir qui fut toujours d'obéir à Votre Excellence. Je regrette beaucoup que, m'ayant convoqué, vous m'ayez trouvé dans le besoin, et que le fait d'avoir à assurer ma subsistance m'ait été un empêchement. Je regrette beaucoup que le fait d'avoir à assurer ma subsistance m'ait empêché de continuer l'œuvre que m'a confié Votre Altesse. Mais j'espère avoir bientôt gagné assez pour pouvoir, l'esprit en paix, satisfaire Votre Excellence à qui je me recommandais »</p><br>
<footer class="blockquote-footer mb-1">Léonard de Vinci à Ludovico Sforza (très utile dans les négociations salariales avec son patron)</footer>
</blockquote><br>

<blockquote class="blockquote"><p>« Le tire-au-cul »</p><br>
<footer class="blockquote-footer mb-1">désigne une personne paresseuse</footer></blockquote><br>

<blockquote class="blockquote"><p>Les « mathématiques sévères »</p><br>
<footer class="blockquote-footer mb-1">Isidore Lucien Ducasse, comte de Lautréamont, <em>Chants de Maldoror</em></blockquote><br>

<blockquote class="blockquote"> « Réfléchir, c’est désobéir ». D’aucuns ajouteraient : « Penser, c’est déserter » </blockquote><br>

<blockquote class="blockquote"><p>« Le pessimisme de la raison, l'optimisme de la volonté »</p><br> <footer class="blockquote-footer mb-1">Gramsci</blockquote><br>

<blockquote class="blockquote"><p>« Sentir un rat est toujours plus facile que de le prendre au piège »</p><br> <footer class="blockquote-footer mb-1">Ludwig Wittgenstein</blockquote><br>

<blockquote class="blockquote"><p>« Fuir, mais en fuyant, chercher une arme »</p><br>
<footer class="blockquote-footer mb-1">Gilles Deleuze</blockquote><br>

<blockquote class="blockquote"><p>« Prendre son cul pour son coeur et croire que la lune est faite pour éclairer son boudoir »</p><br>
<footer class="blockquote-footer mb-1">Gustave Flaubert</blockquote><br>

<blockquote class="blockquote"><p>« Gros Cul, il fait péter l'emmerdomètre »</p><br>
<footer class="blockquote-footer mb-1">Jacques Chirac, alors Premier ministre, à propos du ministre de l'Intérieur Poniatowski</blockquote><br>
