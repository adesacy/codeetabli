---
date: "2024-05-02"
author: "Antoine Silvestre de Sacy"
description: "Algorithmes d'analyse de la complexité littéraire."
paige:
  file_link:
    disable: false
  style: |
    .paige-toc,
    .paige-authors,
    .paige-date,
    .paige-date-header,
    .paige-keywords,
    .paige-reading-time,
    .paige-series,
    .paige-summary {
        display: block;
    }
series: ["complexite"]
tags: ["R", "litterature", "humanités numériques", "code"]
keywords: ["R", "litterature", "humanités numériques", "code"]
title: "Complexité littéraire (2/)"
---


<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complexity.png" %}}</p>
</div>


<br>
<br>

## Présentation

Ce billet s'inscrit dans une série sur les méthodes d'anlayses de la complexité littéraire outillée. Il fait suite au premier module d'annotation et de récupération des indices de complexité textuels que vous pouvez retrouver <a href="https://adesacy.gitpages.huma-num.fr/codeetabli/billets/2024-05-01-complexite_01/">ici</a>. 

Après avoir récupéré les indices de complexité dans le premier module, nous allons maintenant présenter quelques méthodes d'analyse.

## Problématiques

- Quels critères de complexité des textes littéraires peuvent être analysés et récupérés à l'aide de méthodes computationnelles ? 
- Est-ce que les textes canoniques présentent des différences notables par rapport aux textes de l'archive ? Est-ce que, donc, la complexité peut être considérée comme un critère participant au processus de canonisation ?
- Si tel est le cas, quels sont les critères de complexité les plus différenciatifs des textes canoniques et non-canoniques ?
- Est-il possible de classifier les textes littéraires à partir de ces critères de complexité et d'analyser en lecture proches certains passages caractéristiques de cette complexité littéraire ?

## Traitement des données

- Statistiques descriptives sur l'évolution chronologique de ces valeurs.
- Comptage simple et identification des textes les plus/les moins complexes en fonction d'une variable cible (par exemple, quels sont les textes qui ont la plus grande profondeur syntaxique ?)
- Tableau de corrélation entre nos différentes variables de complexité. Un corrrelogramme peut être utilisé à cet effet.
- Classification et visualisation des textes sur la base de ces variables : par exemple, ACP.
- Relier d'autres variables explicatives à nos critères de complexité : par exemple canon / non-canon ; date de publication : nous pourrions utiliser des arbres de décision ici, en demandant lequel de nos critères de complexité influence le plus la prédiction qu'une œuvre appartient à une période particulière, à un genre particulier, etc.

### Statistiques descriptives

Pratiquons quelques statistiques descriptives sur l'évolution chronologique de ces valeurs.


{{< highlight r "linenos=table" >}}

# Load data and metadata : 
data <- read.csv("~/complex/output/data_merged/merged_indices.csv")
metadata <- read.csv("~/complex/data/metadonnees_corpus_chapitres.csv")

# Few cleaning to be able to merge data based on doc_id :

metadata <- metadata %>%
  dplyr::mutate(FileName = str_replace_all(string = FileName, pattern = "\\.xml$", "\\.txt")) %>%
  dplyr::rename(doc_id = FileName)

# Merge data based on doc_id equalities : 

data <- left_join(x = data, y = metadata, by = "doc_id") %>%
  select(-X) 

{{< /highlight >}}

Commençons par nous demander si l'étiquette canonique présente des caractéristiques de complexité plus grande que les oeuvres de l'archive. Nous divisons notre corpus en deux (canon / non-canon) pour ensuite comparer les scores de chacune des variables à l'aide de boîtes à moustache pour chaque variable. Avant cela, nous devons pratiquer une normalisation sur l'ensemble des indicateurs récupérés pour qu'ils soient comparables ; nous optons pour une normalisation z-scores nous permettant de comparer nos valeurs entre elles.

> N.B. : La normalisation z-scores, également appelée standardisation, est une méthode de transformation des données qui consiste à les centrer sur la moyenne et à les diviser par l'écart type. Cette transformation permet de convertir les données dans une échelle standard, indépendante de l'unité de mesure.

{{< highlight r "linenos=table" >}}

data[c(2:7)] <- scale(data[c(2:7)])

data_select <- data %>%
  select(c(1:7,"etiquette")) %>%
  filter(!is.na(etiquette))


library(reshape)
# reshape data for boxplot : 
data_reshape <- melt(data_select)


data_reshape %>%
  ggplot(aes(
    x = variable,
    y = value,
    fill = etiquette,
    color = etiquette
  )) +
  geom_boxplot(position = position_dodge(width = 0.9)) +
  #geom_jitter(size=0.2, alpha=0.2) +
  theme_minimal() +
  theme(legend.position = "right",
        plot.title = element_text(size = 11)) +
  scale_fill_manual(values = c("#E41A1C", "#377EB8")) +
  labs(title = "Boîte à moustaches comparatives canon / non-canon",
       subtitle = "À partir des indices de complexité récupérés",
       caption = "Source : Antoine Silvestre de Sacy") +
  theme(
    # Éléments graphiques des légendes
    plot.title = element_text(
      color = "#000000",
      size = 16,
      face = "bold"
    ),
    plot.subtitle = element_text(size = 10, face = "italic"),
    plot.caption = element_text(face = "italic"),
    axis.text.x = element_text(angle = 45, hjust = 1)
  )

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_boite_moustaches.png" %}}</p>
</div>

<br>
<br>

Nous remarquons d'emblée, à l'aide de cette visualisation comparative très riche, que sur l'ensemble des mesures de complexité que nous avons extraites, les textes canoniques présentent des indicateurs statistiques (moyenne, médiane, max) plus grands que les textes de l'archive, ce qui tend à prouver d'emblée qu'ils se distinguent des textes de l'archive par leur plus grande complexité formelle.
On observe semble-t-il deux groupes distincts d'indicateurs : ceux qui se rapportent proprement à la syntaxe et ceux qui portent davantage sur le lexique (`lexical_richness`, `lemma_richness`) ; ces derniers présentent moins de différences entre canon et archive.


### Tableau de corrélation 

Calculons la corrélation entre nos différentes variables de complexité. Un <a href="https://r-graph-gallery.com/correlogram.html">corrrelogramme</a> peut être utilisé à cet effet.

{{< highlight r "linenos=table" >}}

data_corr <- data %>%
  select("words_per_sentence_mean","lexical_richness","lemma_richness","dependency_length_mean","relative_sub_rate","conjonctive_sub_rate")

# Calcul des corrélations entre les variables
data_corr <- cor(data_corr)

# Créez un corrplot
corrplot(data_corr, method = "circle", type = "full", tl.col = "black", tl.srt = 45, order = "hclust")

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_correlogram.png" %}}</p>
</div>

<br>
<br>

Nous allons maintenant calculer la valeur p (*p-value*) des corrélations. La *p-value* est une mesure essentielle dans le calcul des corrélations, qui permet de déterminer si la relation entre deux variables est statistiquement significative. Elle est utilisée pour évaluer si l'hypothèse nulle selon laquelle il n'y a pas de corrélation entre les variables peut être rejetée.

{{< highlight r "linenos=table" >}}

cor.mtest <- function(mat, ...) {
    mat <- as.matrix(mat)
    n <- ncol(mat)
    p.mat<- matrix(NA, n, n)
    diag(p.mat) <- 0
    for (i in 1:(n - 1)) {
        for (j in (i + 1):n) {
            tmp <- cor.test(mat[, i], mat[, j], ...)
            p.mat[i, j] <- p.mat[j, i] <- tmp$p.value
        }
    }
  colnames(p.mat) <- rownames(p.mat) <- colnames(mat)
  p.mat
}
# matrix of the p-value of the correlation
p.mat <- cor.mtest(data_corr)
p.mat

{{< /highlight >}}

Dans la figure, les corrélations dont la valeur p est > 0,01 sont considérées comme non significatives. Dans ce cas, les valeurs des coefficients de corrélation sont laissées en blanc ou des croix sont ajoutées.

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_correlogram_pvalue.png" %}}</p>
</div>

<br>
<br>

On observe d'emblée à l'aide de ce graphiques deux groupes distincts d'indicateurs : ceux qui portent sur le lexique (les richesses lexicales et lemmatiques) et ceux qui portent sur les critères syntaxiques. Il est intéressant de noter le peu de corrélation entre les scores de diversités lexicales et la variable de longueur des phrases, montrant que des phrases plus longues dans un texte n'impliquent pas une plus grande richesse de vocabulaire - ce qui peut paraître contre-intuitif et qui, à notre connaissance, n'a pas été pointé.

#### Ajout d'autres métadonnées 

La complexité, ou certains indices de complexité, sont-ils liés à la canonicité ?

{{< highlight r "linenos=table" >}}


data_corr_canon <- data %>%
  select("words_per_sentence_mean","lexical_richness","lemma_richness","dependency_length_mean","relative_sub_rate","conjonctive_sub_rate", "etiquette") %>%
  mutate(etiquette = case_when(etiquette == "canon" ~ 1, etiquette == "non-canon" ~ 0)) %>%
  filter(!is.na(etiquette))

# Calcul des corrélations entre les variables
data_corr_canon <- cor(data_corr_canon)

# Créez un corrplot
corrplot(data_corr_canon, method = "circle", type = "full", tl.col = "black", tl.srt = 45, order = "hclust")

{{< /highlight >}}

La corrélation qui apparaît entre canonisation et mesures de complexités syntaxiques confirment nos premières observations que nous avons extraites des visualisations comparatives menées à l'aide des boîtes à moustaches, qui mettaient bien en évidence ces différences au niveau des critères syntaxiques et les moindres différences au regard de la diversité lexicale.

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_correlogram_canon.png" %}}</p>
</div>

<br>
<br>

### Analyse multivariée

#### Analyse en composantes principales

{{< highlight r "linenos=table" >}}

library(factoextra)
library(stats)

# Put title as rowname in ordre to get ind for later viz : 
pca_data <- data %>%
  filter(!is.na(etiquette))
rownames(pca_data) <- pca_data$doc_id

# make PCA on data : 
pca_result <- prcomp(pca_data[,2:7], scale. = F, )

# variable viz : 


fviz_pca_var(pca_result, col.var="contrib",
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE # Avoid text overlapping
             )

{{< /highlight >}}


Très intéressant de voir la significativité des deux composantes, d'une part, proche de 93% (!) et la répartition claire entre un axe lexicale et un axe syntaxique qui sont clairement oppposés dans la réduction de dimensionnalité et le score de corrélation, confirmant une fois encore la différence de nature entre ces critères au niveau statistique. 

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_PCA.png" %}}</p>
</div>

<br>
<br>

On confirme cela en regardant les contributions variables sur deux premières dimensions : 

{{< highlight r "linenos=table" >}}

# --- Contributions : 

# Contributions of variables to PC1
fviz_contrib(pca_result, choice = "var", axes = 1, top = 10)

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_PCA_dim1.png" %}}</p>
</div>

<br>
<br>


{{< highlight r "linenos=table" >}}
# Contributions of variables to PC2
fviz_contrib(pca_result, choice = "var", axes = 2, top = 10)

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_PCA_dim2.png" %}}</p>
</div>

<br>
<br>

Voyons maintenant si les étiquettes canon / archive jouent un rôle dans le regroupement :

{{< highlight r "linenos=table" >}}

fviz_pca_ind(pca_result,
             label = "none", # hide individual labels
             habillage = pca_data$etiquette, # color by groups
             palette = c("#00AFBB", "#E7B800"),
             addEllipses = TRUE # Concentration ellipses
             )

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_PCA-canon.png" %}}</p>
</div>

<br>
<br>

Deux choses ici encore sont intéressantes à souligner : l'absence de significativité de la plupart des résultats qui se concentrent au centre des deux composantes, proches de la valeur 0 ; mais également, la bien plus grande extension des oeuvres canoniques qui présentent des scores élevés de significativité sur les deux axes. Plus précisément, sur la première dimension, les oeuvres canoniques sont les seules à présenter des scores de corrélation négatives importantes ; sur la seconde composante, qui semble correspondre à l'axe lexical, les résultats apparaissent plus mêlés. Cela ne fait que confirmer par ailleurs nos premières analyses de corrélation. 
Il serait intéressant de récupérer les oeuvres de l'archive qui semblent s'extraire du lot. Intéressons-nous à chacun des individus qui sortent du lot dans chacun des axes.


##### Récupération des individus dans l'axe 1

{{< highlight r "linenos=table" >}}

# Get individuals positions for every dim : 
ind <- get_pca(pca_result, element = "ind")

# df : 
ind_df <- as_tibble(ind$coord) %>%
  mutate(doc_id = data_select$doc_id) %>% # add doc_id
  select(c(doc_id, Dim.1)) %>% # select only pertinent columns
  relocate(doc_id, .before = Dim.1) %>% # relocate the var
  mutate(etiquette = data_select$etiquette) %>% # add etiquette
  arrange(Dim.1) # order by descending order (see graph)

{{< /highlight >}}

On peut explorer les résultats ordonnés selon les scores de la première composante : 

{{< highlight r "linenos=table" >}}

DT::datatable(ind_df)

{{< /highlight >}}

On retrouve de façon intéressante, dans les scores les plus bas - qui semblent correspondre aux textes présentant les plus haut degrés de complexité formelle - des textes de Claude Simon et Richard Millet.

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex-table-texts.png" %}}</p>
</div>

<br>
<br>

Regardons sur ce premier axe, que l'on pourrait qualifier d'*axe syntaxique*, les oeuvres qui présentent une significativité (i.e. aux valeurs inférieurs à -1 (Règle de Kaiser)) pour voir leur répartition en termes d'étiquettes canoniques ou non-canoniques.

{{< highlight r "linenos=table" >}}

pca_select <- ind_df %>%
  filter(Dim.1 < -1) %>%
  count(etiquette, sort = T)

pca_select <- pca_select %>% 
  arrange(desc(n)) %>%
  mutate(prop = n / sum(pca_select$n) *100)

# Basic piechart
ggplot(pca_select, aes(x="", y=prop, fill=etiquette)) +
  geom_bar(stat="identity", width=1, color="white") +
  coord_polar("y", start=0) +
  theme_minimal() + 
  theme(legend.position = "right",
        plot.title = element_text(size = 11)) +
  scale_fill_manual(values = c("#E41A1C", "#377EB8")) +
  labs(title = "Répartition comparatives canon / non-canon",
       subtitle = "À partir de l'axe syntaxique (scores < -1) : 70% / 30%",
       caption = "Source : Antoine Silvestre de Sacy") +
  theme(
    # Éléments graphiques des légendes
    plot.title = element_text(
      color = "#000000",
      size = 16,
      face = "bold"
    ),
    plot.subtitle = element_text(size = 10, face = "italic"),
    plot.caption = element_text(face = "italic"),
    axis.text.x = element_text(angle = 45, hjust = 1)
  )


{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_pie-chart-syntax.png" %}}</p>
</div>

<br>
<br>

##### Récupération des individus dans l'axe 2

Pratiquons la même analyse sur l'axe 2. 

{{< highlight r "linenos=table" >}}

# Get individuals positions for every dim : 
ind <- get_pca(pca_result, element = "ind")

# df : 
ind_df <- as_tibble(ind$coord) %>%
  mutate(doc_id = data_select$doc_id) %>% # add doc_id
  select(c(doc_id, Dim.2)) %>% # select only pertinent columns
  relocate(doc_id, .before = Dim.2) %>% # relocate the var
  mutate(etiquette = data_select$etiquette) %>% # add etiquette
  arrange(desc(Dim.2)) # order by descending order (see graph)

{{< /highlight >}}

Regardons sur ce deuxième axe, que l'on pourrait qualifier d'*axe lexical*, les oeuvres qui présentent une significativité pour voir leur répartition en termes d'étiquettes canoniques ou non-canoniques en analysant les scores de corrélations supérieurs à 1 dans ce deuxième axe vertical.

{{< highlight r "linenos=table" >}}

pca_select <- ind_df %>%
  filter(Dim.2 > 1) %>%
  count(etiquette, sort = T)

pca_select <- pca_select %>% 
  arrange(desc(n)) %>%
  mutate(prop = n / sum(pca_select$n) *100)

# Basic piechart
ggplot(pca_select, aes(x="", y=prop, fill=etiquette)) +
  geom_bar(stat="identity", width=1, color="white") +
  coord_polar("y", start=0) +
  theme_minimal() + 
  theme(legend.position = "right",
        plot.title = element_text(size = 11)) +
  scale_fill_manual(values = c("#E41A1C", "#377EB8")) +
  labs(title = "Répartition comparatives canon / non-canon",
       subtitle = "À partir de l'axe lexical (scores > 1) : 47% / 52%.",
       caption = "Source : Antoine Silvestre de Sacy") +
  theme(
    # Éléments graphiques des légendes
    plot.title = element_text(
      color = "#000000",
      size = 16,
      face = "bold"
    ),
    plot.subtitle = element_text(size = 10, face = "italic"),
    plot.caption = element_text(face = "italic"),
    axis.text.x = element_text(angle = 45, hjust = 1)
  )

{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex_pie-chart-lexical.png" %}}</p>
</div>

<br>
<br>

Regardons maintenant la répartition des valeurs inférieures à -1 sur ce même axe lexical.

{{< highlight r "linenos=table" >}}

pca_select <- ind_df %>%
  filter(Dim.2 < -1) %>%
  count(etiquette, sort = T)

pca_select <- pca_select %>% 
  arrange(desc(n)) %>%
  mutate(prop = n / sum(pca_select$n) *100)

# Basic piechart
ggplot(pca_select, aes(x="", y=prop, fill=etiquette)) +
  geom_bar(stat="identity", width=1, color="white") +
  coord_polar("y", start=0) +
  theme_minimal() + 
  theme(legend.position = "right",
        plot.title = element_text(size = 11)) +
  scale_fill_manual(values = c("#E41A1C", "#377EB8")) +
  labs(title = "Répartition comparatives canon / non-canon",
       subtitle = "À partir de l'axe lexical (scores < -1) : 39% / 61%.",
       caption = "Source : Antoine Silvestre de Sacy") +
  theme(
    # Éléments graphiques des légendes
    plot.title = element_text(
      color = "#000000",
      size = 16,
      face = "bold"
    ),
    plot.subtitle = element_text(size = 10, face = "italic"),
    plot.caption = element_text(face = "italic"),
    axis.text.x = element_text(angle = 45, hjust = 1)
  )


{{< /highlight >}}

<br>
<br>

<div style="display: block; margin-left: auto; margin-right: auto; text-align: center;">
{{% paige/image alt="Landscape"
    width="32rem" imageclass="rounded-4 shadow img-fluid"
    stretch=cover
    src="complex-pie-chart-lexical-2.png" %}}</p>
</div>

<br>
<br>

Que ce soit au niveau des scores de corrélation négatifs ou positifs, nous observons une grande majorité de textes non-canoniques, ce qui tend à montrer l'impossibilité d'affirmer que les textes canoniques présentent une plus importante richesse de vocabulaire - cela tendrait même à montrer le contraire. Cela semble également confirmer l'absence de corrélation entre les indices de complexités lexicales et la canonicité.

70% de textes canoniques pour 30% de textes de l'archives dans ce qui semble être l'axe syntaxique de complexité présentant des scores de corrélations élevés entre les textes.

